from django.urls import path
from . import views

# Adding a namespace to this urls.py helps django distinguish this set of routes from other urls.py files in other packages.
app_name = 'todolist'
urlpatterns = [
	path('', views.index, name="index"),
	# localhost:8000/todolist/1
	path('<int:todoitem_id>/', views.todoitem, name="viewtodoitem"),
	path('<int:event_id>/', views.eventdetails, name="vieweventdetails"),
	path('register/', views.register, name="register"),
	path('change-password/', views.change_password, name="change_password"),
	path('login/', views.login_view, name="login"),
	path('logout/', views.logout_view, name="logout"),
	path('task/add/', views.add_task, name="add_task"),
	path('task/<int:todoitem_id>/edit', views.update_task, name="update_task"),
	path('task/<int:todoitem_id>/delete', views.delete_task, name="delete_task"),
	path('event/add/', views.add_event, name="add_event"),
	path('event/<int:event_id>/edit', views.update_event, name="update_event"),
	path('event/<int:event_id>/delete', views.delete_event, name="delete_event"),
	path('update-profile/', views.update_profile, name="update_profile")
]	